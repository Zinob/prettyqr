This program creates wall-paper style QR-codes.

They might be a bit hard to scan so unless you feel like people should spend a minute or so wiggling their cellphones to get it to scan you better use a rather square image.

While the program support QR codes of size up to v40 or 2331 bytes of data; QR codes containing more than 26 bytes will be rather hard to scan using the default flower pattern.

The program depends on the elaphe and the PIL packages (get them via pip install elaphe). This means that unless you already have them installed it is  a pretty hefty download for such a small function.

I have included a flower from the Ms Windings font which should be free to use according to [Wikipedia](http://en.wikipedia.org/wiki/Wingdings "Wingdings - Wikipedia, the free encyclopedia"). It requires quite a lot of camera wiggling to get it to scan and might not be optimal but I got the idea for this project from a wallpaper that sort-of looked like that.

Usage example:  ./prettyQR.py http://zxz.se black.png out.png  
would produce an output like:  
![rendering of above command](https://bytebucket.org/Zinob/prettyqr/raw/master/example.png)
