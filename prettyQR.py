#!/usr/bin/python
from elaphe import barcode
from sys import stdout,argv
from PIL import Image,ImageStat


def genCode(text,version):
	codeImage=barcode('qrcode',text,options=dict(version=version, eclevel='M'),margin=0, data_mode='8bits')
	return codeImage

def calulateVersion(str):
	"""returns the minimum QR version level which can fit the supplied string at M-encoding"""
	#capacity for M-lever qr-codes source:http://www.thonky.com/qr-code-tutorial/character-capacities/
	capa=[-1,14,26,42,62,84,106,122,152,180,213,251,287,331,362,412,450,504,560,624,666,711,779,857,911,997,1059,1125,1190,1264,1370,1452,1538,1628,1722,1809,1911,1989,2099,2213,2331]
	l=len(str)
	for n,c in enumerate(capa):
		if c>= l:
			return n

def getGridWidth(img):
	"""Attempts to figgure out how many pixels wide each QR-module is"""
	xDim,yDim=img.size
	minW=100
	for y in range(0,yDim,yDim/10):
		lastCol=255
		lastCol=img.getpixel((1,y))[0]
		for x in range(0,xDim-1):
			thisCol=img.getpixel((x,y))[0]
			lastSwitch=0
			if lastCol!=thisCol:
				width=x-lastSwitch
				lastCol=thisCol
				lastSwitch=x
				#stdout.write("." if thisCol else "x")
				if width<minW:
					minW=width
			#else:
				#stdout.write(" " if thisCol else "#")
	return minW

def genMatrix(text):
	"""Transforms the supplied text to a QR matrix of  0,1 and 2
	where 0 is the user supplied image,
	1 is white and
	2is "true black" use for the function patterns"""
	version=calulateVersion(text)
	img=genCode(text,version)
	pxwidth=getGridWidth(img)
	xDim,yDim=img.size
	pix=[]
	for y in range(0,yDim,pxwidth):
		row=[]
		for x in range(0,xDim,pxwidth):
			#print img.getpixel((x,y))[0]/255,
			row.append(img.getpixel((x,y))[0]/255)
		pix.append(row)
	yDim=len(pix)
	xDim=len(pix[0])
	drawXSyncPattern(pix)
	drawYSyncPattern(pix)
	drawSepparationMarker(pix,(0,0))
	drawSepparationMarker(pix,(0,yDim-7))
	drawSepparationMarker(pix,(xDim-7,0)),
	drawAlignmentMarkers(pix, version)
	return pix

def drawXSyncPattern(mat):
	mat[6]=([2,1]*(len(mat[0])/2+1))[:len(mat[0])]

def drawYSyncPattern(mat):
	n=0
	for i in range(len(mat)):
		mat[i][6]=(i+1)%2+1

def drawSepparationMarker(mat,pos):
	"""Draws a finder pattern with its upper left corner at the supplied coordinates"""
	marker=[[2]*7,
			[2,1,1,1,1,1,2],
			[2,1,2,2,2,1,2],
			[2,1,2,2,2,1,2],
			[2,1,2,2,2,1,2],
			[2,1,1,1,1,1,2],
			[2]*7]
	blitToMatrix(mat,marker,pos)
	
def drawAlignmentMarkers(mat,version):
	"""draws the alignment markers.
	assumes that the drawAlingmnetMarker function is a noop if the possition is overlapping other markers"""
	#source:http://www.thonky.com/qr-code-tutorial/alignment-pattern-locations/
	possitions=[[],[], [6,18], [6,22], [6,26], [6,30], [6,34], [6,22,38], [6,24,42], [6,26,46],#v1-9
			[6,28,50], [6,30,54], [6,32,58], [6,34,62], [6,26,46,66], [6,26,48,70], [6,26,50,74],#v10-16
			[6,30,54,78], [6,30,56,82], [6,30,58,86], [6,34,62,90], [6,28,50,72,94],#v17-21
			[6,26,50,74,98], [6,30,54,78,102], [6,28,54,80,106], [6,32,58,84,110], [6,30,58,86,114],#v22-26
			[6,34,62,90,118], [6,26,50,74,98,122], [6,30,54,78,102,126], [6,26,52,78,104,130],#v27-30
			[6,30,56,82,108,134], [6,34,60,86,112,138], [6,30,58,86,114,142], [6,34,62,90,118,146],#v31-34
			[6,30,54,78,102,126,150], [6,24,50,76,102,128,154], [6,28,54,80,106,132,158],#v35-37
			[6,32,58,84,110,136,162], [6,26,54,82,110,138,166], [6,30,58,86,114,142,170]]#v38-40

	for y in possitions[version]:
		for x in possitions[version]:
			drawAlignmentMarker(mat,(x,y))

def drawAlignmentMarker(mat,pos):
	"""Draws an alignment marker with its center at the supplied coordinates.
	No attempts are made to ensure that the possition is valid."""
	marker=[[2]*5,
			[2,1,1,1,2],
			[2,1,2,1,2],
			[2,1,1,1,2],
			[2]*5]
	x,y=pos[0]-2,pos[1]-2
	xDim,yDim=len(mat[0]), len(mat)
	if not (x<10 and (y<10 or (yDim-y)< 10) or (y<10 and (xDim-x)< 10) ):
		blitToMatrix(mat,marker,(x,y))
	
def blitToMatrix(dest,src,pos):
	"""Copies the supplied src matrix with its top left corner achored to a suppoled position in the destination matrix"""
	for x in range(len(src[0])):
		for y in range(len(src)):
			dest[y+pos[1]][x+pos[0]]=src[y][x]

def genPretty(text,customBlack,outfile):
	mat=genMatrix(text)
	xDim=len(mat[0])
	yDim=len(mat)

	custom=Image.open(customBlack)
	pxXDim=custom.size[0];
	pxYDim=custom.size[1];
	#avgColour="black" #my QR-scanners perfer if the allingment patters have the same brightness as the data pattern, this might varry..
	avgColour="#%02x%02x%02x"%tuple(ImageStat.Stat(custom).mean[:3])
	black=Image.new("RGB", (pxXDim,pxYDim), avgColour)
	target=Image.new("RGB", (pxXDim*xDim,pxYDim*yDim), "white")
	for y in range(yDim):
		for x in range(xDim):
			if mat[y][x]==2:
				px=black
				target.paste(black,(x*pxXDim,y*pxYDim))
			elif mat[y][x]==0:
				px=custom
				target.paste(custom,(x*pxXDim,y*pxYDim))
	target.save(outfile)
#a=genCode("foo")
#a.save("foo.gif")
#print "\n".join(map(str,genMatrix("foo")))
if len(argv)==4:
	genPretty(argv[1],argv[2],argv[3])
else:
	print "Error usage is "+ argv[0] + " Text black-pixel-file.fmt outfile.fmt"
	print "where Text is 1 to 26 characters long"
